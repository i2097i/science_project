// g++ -o main main.cpp -lwiringPi

#include <iostream>
#include <signal.h>
#include <stdlib.h> //atexit
#include <stdio.h>
#include <wiringPi.h>
#include <math.h>

using namespace std;

#define SPEED_OF_SOUND 339.71
#define FEET_PER_METER 3.28084
#define SECONDS_PER_MICROSECOND 0.000001
#define LIGHT_COUNT 8
#define MAX_RANGE_FT 5

#define TRIG 0
#define ECHO 1

#define L1 2
#define L2 3
#define L3 4
#define L4 5
#define L5 6
#define L6 7
#define L7 8
#define L8 9

int lights[LIGHT_COUNT] = {L1, L2, L3, L4, L5, L6, L7, L8};
bool isConfiguring = false;

void configureLights(int microSeconds);
float meters(int microSeconds);
float feet(float meters);
float inches(float feet);

void onExit(int s);

int main(void) {
  signal(SIGINT, onExit);
  wiringPiSetup();
  pinMode(TRIG, OUTPUT);
  pinMode(ECHO, INPUT);

  for (int i = 0; i < LIGHT_COUNT; i++) {
    pinMode(lights[i], OUTPUT);
    digitalWrite(lights[i], HIGH);
  }

  while(1) {
    digitalWrite(TRIG, LOW);
    delay(30);
    //Send trig pulse
    digitalWrite(TRIG, HIGH);
    delayMicroseconds(20);
    digitalWrite(TRIG, LOW);
    //Wait for echo start
    while(digitalRead(ECHO) == LOW);

    //Wait for echo end
    long startTime = micros();
    while(digitalRead(ECHO) == HIGH);
    long travelTime = (micros() - startTime) / 2;
    float m = meters(travelTime);
    float ft = feet(m);
    float in = inches(ft);

    configureLights(travelTime);
    delayMicroseconds(75000);
  }
  return 0;
}

void configureLights(int microSeconds) {
  if (isConfiguring) {
    return;
  }

  isConfiguring = true;
	float m = meters(microSeconds);
	float ft = feet(m);
	float in = inches(ft);

  float percent = in / (float) (MAX_RANGE_FT * 12);
  if (percent > 1.0) {
    percent = 1.0;
  }
  int offCount = (int) round((float)LIGHT_COUNT * percent);

  for (int i = LIGHT_COUNT; i >= 0; i--) {
    if (i < LIGHT_COUNT - offCount) {
      digitalWrite(lights[i], LOW);
    } else {
      digitalWrite(lights[i], HIGH);
    }
  }

  isConfiguring = false;
}

float meters(int microSeconds) {
	return microSeconds * SECONDS_PER_MICROSECOND * SPEED_OF_SOUND;
}

float feet(float meters) {
	return meters * FEET_PER_METER;
}

float inches(float feet) {
	return feet * 12.0;
}

void onExit(int s) {
   cout << "Exiting..." << endl;
   for (int i = 0; i < 8; i++) {
      digitalWrite(lights[i], HIGH);
   }
   exit(0);
}
